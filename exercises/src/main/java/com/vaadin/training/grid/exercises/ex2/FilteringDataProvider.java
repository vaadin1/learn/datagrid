package com.vaadin.training.grid.exercises.ex2;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.router.Route;
import com.vaadin.training.grid.exercises.MainLayout;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Route(value = FilteringDataProvider.ROUTE, layout = MainLayout.class)
public class FilteringDataProvider extends Composite<VerticalLayout>{

	public static final String ROUTE = "ex2";
	public static final String TITLE = "Exercise 2";

	private final ListDataProvider<Product> dataProvider;

	public FilteringDataProvider() {
		final VerticalLayout layout = getContent();
		layout.setWidth("100%");

		dataProvider = DataProviderHelper.createProductDataProvider();
		dataProvider.setSortOrder(Product::getAvailable, SortDirection.DESCENDING);

		DatePicker dtStart = new DatePicker("Start");
		DatePicker dtEnd = new DatePicker("End");
		Button btnFilter = new Button("Filter", event -> {
			if (!(dtStart.getValue() != null || dtEnd.getValue() != null))
				dataProvider.clearFilters();
			else
				dataProvider.setFilter(Product::getAvailable, available -> filterProduct(available, dtStart.getValue(), dtEnd.getValue()));
		});

		HorizontalLayout horizontalLayout = new HorizontalLayout(dtStart, dtEnd, btnFilter);
		horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.END);

		// TODO create and populate Grid
		Grid<Product> grid = new Grid<>();
		grid.setItems(dataProvider);
		grid.addColumn(prod -> prod.getAvailable().format(DateTimeFormatter.ofPattern("MMMM dd, yyyy"))).setHeader("Available");
		grid.addColumn(Product::getName).setHeader("Name");
		grid.addColumn(Product::getPrice).setHeader("Price");
		layout.add(horizontalLayout, grid);
	}

	private boolean filterProduct(LocalDate available, LocalDate start, LocalDate end) {
		if (available == null) {
			// If 'available' date is null, return true only if both start and end are null
			return start == null && end == null;
		}

		// If only start is null, check if 'available' is on or before 'end'
		if (start == null) {
			return !available.isAfter(end);
		}

		// If only end is null, check if 'available' is on or after 'start'
		if (end == null) {
			return !available.isBefore(start);
		}

		// If both start and end are non-null, check if 'available' is between or equal to them
		return !available.isBefore(start) && !available.isAfter(end);
	}


}
