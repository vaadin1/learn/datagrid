package com.vaadin.training.grid.exercises.ex3;


import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.*;
import com.vaadin.flow.router.Route;
import com.vaadin.training.grid.exercises.MainLayout;
import com.vaadin.training.grid.exercises.ex1.Person;
import com.vaadin.training.grid.exercises.ex2.DataProviderHelper;
import com.vaadin.training.grid.exercises.ex2.Product;

import java.util.ArrayList;
import java.util.List;

@Route(value = BackEndDataProvider.ROUTE, layout = MainLayout.class)
public class BackEndDataProvider extends VerticalLayout {
	private static final long serialVersionUID = 1L;

	public static final String ROUTE = "ex3";
	public static final String TITLE = "Exercise 3";

	final PersonService service = new PersonService();

	public BackEndDataProvider() {
		setWidth("100%");

		final List<AgeGroup> groups = new ArrayList<>();
		groups.add(new AgeGroup(0, 18));
		groups.add(new AgeGroup(19, 26));
		groups.add(new AgeGroup(27, 40));
		groups.add(new AgeGroup(41, 100));

		final ComboBox<AgeGroup> cmbFilter = new ComboBox<>("Filter", groups);
		add(cmbFilter);

		final Grid<Person> grid = new Grid<>();
		grid.setWidth("90%");
		add(grid);

		CallbackDataProvider<Person, AgeGroup> dataProvider = DataProvider.fromFilteringCallbacks(
				query -> service.getPersons(query.getOffset(), query.getLimit(), query.getFilter().orElse(null)),
				query -> service.countPersons(query.getOffset(), query.getLimit(), query.getFilter().orElse(null))
		);

		ConfigurableFilterDataProvider<Person, Void, AgeGroup> filterDataProvider = dataProvider.withConfigurableFilter();

		grid.addColumn(Person::getName).setHeader("Name").setKey("name");
		grid.addColumn(Person::getEmail).setHeader("Email").setKey("email");
		grid.addColumn(Person::getAge).setHeader("Age").setKey("age");
		grid.addColumn(Person::getBirthday).setHeader("Birthday").setKey("birthday");

		grid.setDataProvider(filterDataProvider);
		cmbFilter.addValueChangeListener(event -> filterDataProvider.setFilter(event.getValue()));
	}
}